using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.DependencyInjection;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using System.Collections.Generic;
using System.Reflection;
using System.Net;

namespace ContentAPIv3.HealthChecks
{

    public interface IFilePathHealthCheckHelper
    {
             Task<HealthCheckResult> VerifyFilePath(string path, HealthStatus failureStatus = HealthStatus.Unhealthy);

             bool DirectoryExists(string path);
        
    }

    /// <summary>
    /// Verify the provided path exists
    /// </summary>
    public class FilePathHealthCheck : IHealthCheck
    {
        public string Path { get; }
        private IFilePathHealthCheckHelper Helper { get; }

        public FilePathHealthCheck(string path): this(path, new FilePathHealthCheckHelper())
        {
        }

        public FilePathHealthCheck(string path, IFilePathHealthCheckHelper helper)
        {
            Path = path;
            Helper = helper;
        }

        public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default(CancellationToken))
        {
            return Helper.VerifyFilePath(Path, context.Registration.FailureStatus).Result;
        }



    }


    public class FilePathHealthCheckHelper : IFilePathHealthCheckHelper
    {
        
        public async Task<HealthCheckResult> VerifyFilePath(string Path, HealthStatus failureStatus = HealthStatus.Unhealthy)
        {
            HealthCheckResult result;
            string serverIP = "";

            try
            {
                serverIP = Dns.GetHostEntry(Dns.GetHostName()).AddressList[0].ToString();
            }
            catch (System.Exception)
            {
                
                serverIP = Dns.GetHostName();
            }

            string description = string.Format("Verifying path '{0}'. (server: {1}, build date: {2})", Path, serverIP, File.GetLastWriteTime(Assembly.GetExecutingAssembly().Location));

            if (DirectoryExists(Path))
                result = HealthCheckResult.Healthy(description);

            else
                result = new HealthCheckResult(status: failureStatus, 
                            description: description);

            return result;
        }

        public virtual bool DirectoryExists(string path)
        {
            return Directory.Exists(path);
        }

    }


    /// <summary>
    /// Extension methods to assist with DI for the health check
    /// </summary>
     public static class FilePathHealthCheckBuilderExtensions
    {
        private const string NAME = "FilePath";

        public static IHealthChecksBuilder AddFilePathHealthCheck(
            this IHealthChecksBuilder builder,
            string path, 
            string name = default,
            HealthStatus? failureStatus = null,
            IEnumerable<string> tags = null,
            long? thresholdInBytes = null)
        {
            return builder.Add(
                new HealthCheckRegistration( name ?? NAME,
                sp => new FilePathHealthCheck( path ),
                failureStatus,
                tags)
            );
        }
    }
}